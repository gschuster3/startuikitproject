//
//  randomSquare.m
//  bouncingSquares
//
//  Created by GeraldSchuster on 2/17/14.
//  Copyright (c) 2014 Gerald Schuster. All rights reserved.
//

#import "randomSquare.h"
#import "GHSViewController.h"

@implementation randomSquare

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [self randomColor];
        self.frame = [self randomRect];
    
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (CGRect )randomRect
{
    
    CGRect recOne = CGRectMake(0, 0, 25, 25);
    
    return recOne;
    
}

- (UIColor *)randomColor
{
    CGFloat red =  ((arc4random() % 100 / 100.0));
    CGFloat green =  ((arc4random() % 100 / 100.0));
    CGFloat blue =  ((arc4random() % 100 / 100.0));
    
    UIColor *newColor = [UIColor colorWithRed:red green:green blue:blue alpha:1];
    
    
    return newColor;
}



@end
