//
//  PropertiesViewController.m
//  UIKitExamplesForPresentation
//
//  Created by GeraldSchuster on 2/18/14.
//  Copyright (c) 2014 Gerald Schuster. All rights reserved.
//

#import "PropertiesViewController.h"

@interface PropertiesViewController ()


@end

@implementation PropertiesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
